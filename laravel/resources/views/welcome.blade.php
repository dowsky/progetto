<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inizio</title>
        <link rel="stylesheet" href="{{ asset('css/hamburger.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    </head>
    <body>
        <div>
            <nav>
                <div>
                    <h1 class="h1">Progetto</h1>
                </div>
            </nav>
                <div class="hamburger">
                    <div class="line1"></div>
                    <div class="line2"></div>
                    <div class="line3"></div>
                </div>
                <div class="nav-links">
                    <a href="#">home</a>
                    <a href="#">Contact us</a>
                </div>
        </div>
    </body>
    <script src="{{ asset('js/hamburger.js') }}"></script>
</html>
